<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>


## About the project

Application made using laravel + passport to control access to apis.
Laravel Passport provides a complete OAuth2 server implementation, as a way of authenticating this project uses client credentials since we don't have user registration in the application.



## Functionalities


The application has 2 basic crud that aims to control and display what is the percentage of organic chocolate from each batch and producer in a givenchocolate bar.The registration of chocolate bars can be accessed via a public api

## Getting started

##### Install all the dependencies using composer
    composer install
Copy the example env file and make the required configuration changes in the .env file
cp .env.example .env

#### Make sure you set the correct database connection information before running the migrations
##### Run the database migrations (Set the database connection in .env before migrating)
    php artisan migrate
    
##### Before using Laravel's encrypter, you must set a key option in your config/app.php configuration file. You should use the php artisan key:generate command to generate this key
    php artisan key:generate
    

##### Install passport laravel 
    php artisan passport:install
    
##### Run the laravel passport to obtain an access key to authenticate in the application's apis senseis.
    php artisan passport:client --client
 
when run the command in the terminal will ask for a client name to generate a Client secret (access key) and client id
remembering that the secret must be copied for use in insomnia or elsewhere.
Don't forget to copy the secret as it is saved in the encrypted database
    
    
##### Run (seed) for initial data feed for company data and cacau origin.
    php artisan db:seed

##### Start the local development server.
    php artisan serve


