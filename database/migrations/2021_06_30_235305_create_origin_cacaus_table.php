<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOriginCacausTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('origin_cacaus', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('type');
            $table->mediumText('description');
//            $table->foreign('id')->references('origen')->on('producers_cacaus');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('origin_cacaus');
    }
}
