<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLotsChocolateBarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lots_chocolate_bars', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_barra');
            $table->decimal('quantity_grams');
            $table->decimal('quantity_percentage');
            $table->string('deleted')->nullable();
            $table->integer('producer_id');
            $table->integer('batch_id');
            $table->timestamps();

            $table->foreign('id_barra')->references('id')->on('chocolate_bars')->onDelete('cascade');;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lots_chocolate_bars');
    }
}
