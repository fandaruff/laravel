<?php

namespace Database\Seeders;

use App\Models\ProducersCacau;
use Illuminate\Database\Seeder;

class ProducersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $producers = [
            [
                'origen' => 1,
                'name' => 'RZM Kakau',
                'city' => 'São João Batista',
                'description' => 'produtor de cacau orgânico'
            ],
            [
                'origen' => 1,
                'name' => 'RZM Organic:',
                'city' => 'Tijucas',
                'description' => 'produtor de cacau orgânico'
            ],
            [
                'origen' => 2,
                'name' => 'RZM Foods Brazil:',
                'city' => 'Canelinha',
                'description' => 'produtor de cacau pré-processado (pasta base)'
            ],
        ];


        foreach($producers as $producer){
            ProducersCacau::create($producer);
        }
    }
}
