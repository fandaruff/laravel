<?php

namespace Database\Seeders;

use App\Models\OriginCacau;
use Illuminate\Database\Seeder;

class OrigenSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $origens = [
            [
                'id' => 1,
                'type' => 'cacau orgânico',
                'description' => 'produzido com as sementes do cacau orgânico'
            ],
            [
                'id' => 2,
                'type' => 'cacau pré-processado.',
                'description' => 'pré-processamento do fruto, que compreende a colheita e abertura do mesmo'
            ],
        ];


        foreach($origens as $origen){
            OriginCacau::create($origen);
        }

    }
}
