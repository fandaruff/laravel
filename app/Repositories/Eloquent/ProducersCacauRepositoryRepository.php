<?php


namespace App\Repositories\Eloquent;


use App\Models\ProducersCacau;
use App\Repositories\Contracts\ProducersCacauRepositoryInterface;

class ProducersCacauRepositoryRepository extends AbstractRepository implements ProducersCacauRepositoryInterface
{
    protected $model = ProducersCacau::class;
}
