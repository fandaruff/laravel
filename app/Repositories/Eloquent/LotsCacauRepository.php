<?php


namespace App\Repositories\Eloquent;




use App\Models\LotsCacau;
use App\Repositories\Contracts\LotsCacauRepositoryInterface;

class LotsCacauRepository extends AbstractRepository implements LotsCacauRepositoryInterface
{
    protected $model = LotsCacau::class;
}
