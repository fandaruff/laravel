<?php


namespace App\Repositories\Eloquent;

use App\Models\LotsChocolateBars;
use App\Repositories\Contracts\LotsCacauRepositoryInterface;
use App\Repositories\Contracts\LotsChocolateBarsRepositoryInterface;

class LotsChocolateBarsRepository extends AbstractRepository implements LotsChocolateBarsRepositoryInterface
{
    protected $model = LotsChocolateBars::class;
}
