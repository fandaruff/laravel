<?php


namespace App\Repositories\Eloquent;


use App\Models\ChocolateBars;
use App\Repositories\Contracts\ChocolateBarsRepositoryInterface;

class ChocolateBarsRepository extends AbstractRepository implements ChocolateBarsRepositoryInterface
{
    protected $model = ChocolateBars::class;
}
