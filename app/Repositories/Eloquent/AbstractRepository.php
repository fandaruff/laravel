<?php


namespace App\Repositories\Eloquent;


abstract class AbstractRepository
{

    protected $model;

    public function __construct()
    {
       $this->model = $this->resolveModel();
    }

    public function all()
    {
        return $this->model->all();
    }

    public function find($id)
    {
        return $this->model->find($id);
    }

    public function findBy($att, $column)
    {

        return  $this->model->where($att, $column)->get();




    }

    public function store($request)
    {
        return  $this->model->create($request);

    }

    public function update($request)
    {
        $id = $this->model->where('id', $request['id'])->update($request);
        return  $this->find($id);

    }

    public function destroy($id)
    {

        $response =  $this->model->destroy($id);
        return $response == 1 ? "Apagado com sucesso" : "Item informado não existe para ser apagado";

    }

    protected function resolveModel()
    {
        return app($this->model);
    }


    public function updateOrCreate($verify_id, $request)
    {
        return  $this->model->updateOrCreate($verify_id, $request);
    }


    /*
        sorry for this code snippet, I'm out of time because it's still working
     */
    public function findBarInfor($id)
    {
        return \DB::select("SELECT
                                    lots.quantity_grams,
                                    lots.quantity_percentage,
                                    prod.name as procedurs,
                                    origin.type as origin
                                        FROM lots_chocolate_bars as lots
                                        LEFT JOIN producers_cacaus as prod ON prod.id = lots.producer_id
                                        LEFT JOIN origin_cacaus as origin ON origin.id = prod.origen
                                        where id_barra = $id");
    }




}
