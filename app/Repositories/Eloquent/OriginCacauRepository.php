<?php


namespace App\Repositories\Eloquent;


use App\Models\OriginCacau;
use App\Repositories\Contracts\OriginCacauRepositoryInterface;

class OriginCacauRepository extends AbstractRepository implements OriginCacauRepositoryInterface
{
    protected $model = OriginCacau::class;
}
