<?php


namespace App\Repositories\Contracts;




interface TesteRepositoryInterface

{
    public function all();
    public function find($id);
    public function findBy($att, $column);
}

