<?php


namespace App\Repositories\Contracts;


interface OriginCacauRepositoryInterface
{
    public function all();
    public function find($id);
}
