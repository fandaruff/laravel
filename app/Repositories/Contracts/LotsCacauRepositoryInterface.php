<?php


namespace App\Repositories\Contracts;




interface LotsCacauRepositoryInterface

{
    public function all();
    public function find($id);
    public function findBy($att, $column);
    public function store($request);
    public function update($request);
    public function destroy($id);
}

