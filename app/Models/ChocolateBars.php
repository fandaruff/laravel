<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ChocolateBars extends Model
{
    protected $table = "chocolate_bars";
    protected $fillable = ['id', 'gram', 'public_identifier_code', 'created_at', 'updated_at'];
}
