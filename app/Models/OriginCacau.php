<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OriginCacau extends Model
{
    protected $table = "origin_cacaus";
    protected $fillable = ['id', 'type', 'description', 'created_at', 'updated_at'];


    public function producers()
    {
        return $this->hasMany( ProducersCacau::class, 'origen', 'id');
    }
}
