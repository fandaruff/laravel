<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LotsChocolateBars extends Model
{
    protected $table = "lots_chocolate_bars";
    protected $fillable = ['id', 'id_barra', 'quantity_grams', 'quantity_percentage','producer_id', 'batch_id' ,'deleted' ,'created_at', 'updated_at'];
}
