<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProducersCacau extends Model
{
    protected $table = "producers_cacaus";
    protected $fillable = ['id', 'name', 'city', 'origen', 'description', 'created_at', 'updated_at'];


    public function origen()
    {
        return $this->belongsTo(OriginCacau::class, 'id', 'origen');
    }
}
