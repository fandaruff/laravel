<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LotsCacau extends Model
{
    protected $table = "lots_cacaus";
    protected $fillable = ['id', 'description', 'id_producer', 'quantity_grams', 'created_at', 'updated_at'];
}
