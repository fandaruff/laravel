<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Laravel\Passport\Passport;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //

        $this->app->bind(
            'App\Repositories\Contracts\TesteRepositoryInterface',
            'App\Repositories\Eloquent\TesteRepository');

        $this->app->bind(
            'App\Repositories\Contracts\LotsCacauRepositoryInterface',
            'App\Repositories\Eloquent\LotsCacauRepository');

        $this->app->bind(
        'App\Repositories\Contracts\ProducersCacauRepositoryInterface',
            'App\Repositories\Eloquent\ProducersCacauRepositoryRepository');

        $this->app->bind(
            'App\Repositories\Contracts\ChocolateBarsRepositoryInterface',
            'App\Repositories\Eloquent\ChocolateBarsRepository');

        $this->app->bind(
            'App\Repositories\Contracts\LotsChocolateBarsRepositoryInterface',
            'App\Repositories\Eloquent\LotsChocolateBarsRepository');

        $this->app->bind(
            'App\Repositories\Contracts\OriginCacauRepositoryInterface',
            'App\Repositories\Eloquent\OriginCacauRepository');

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        Passport::hashClientSecrets();
    }
}
