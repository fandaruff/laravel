<?php


namespace App\Service;


use App\Repositories\Contracts\LotsCacauRepositoryInterface;
use App\Repositories\Contracts\ProducersCacauRepositoryInterface;

class LotsCacauService
{

    protected $LotsCacauModel;
    protected $ProducersCacauModel;

    public function __construct(LotsCacauRepositoryInterface $LotsCacauModel, ProducersCacauRepositoryInterface $ProducersCacauModel)
    {
        $this->LotsCacauModel = $LotsCacauModel;
        $this->ProducersCacauModel = $ProducersCacauModel;
    }

    public function lotsCheck($request)
    {
        if ($this->ProducersCacauModel->find($request->id_producer)) {
            $response = $this->updateOrCreateLotsCacau($request);
            return $response;
        } else {
            return $this->mountReturn("Produtor informado não existe.", '', 200);
        }
    }

    public function updateOrCreateLotsCacau($request)
    {
        if (!empty($request->id)) {
            $response = $this->LotsCacauModel->update($request->all());
            $menssage = 'Alterado com sucesso';
        } else {
            $response = $this->LotsCacauModel->store($request->all());
            $menssage = 'Cadastrado com sucesso.';
        }
        return $this->mountReturn($menssage, $response->getAttributes(), 200);
    }

    public function destroyLotsCacau($id)
    {
        $message = $this->LotsCacauModel->destroy($id);
        return $this->mountReturn($message, '', 200);
    }

    public function mountReturn($message, $response, $status)
    {
        return [
            'message' => $message,
            'response' => $response,
            'status' => $status
        ];
    }

}
