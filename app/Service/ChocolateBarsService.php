<?php


namespace App\Service;


use App\Models\ChocolateBars;
use App\Repositories\Contracts\ChocolateBarsRepositoryInterface;
use App\Repositories\Contracts\LotsCacauRepositoryInterface;
use App\Repositories\Contracts\LotsChocolateBarsRepositoryInterface;
use App\Repositories\Contracts\ProducersCacauRepositoryInterface;
use Illuminate\Support\Str;

class ChocolateBarsService
{

    protected $chocolateModel;
    protected $lotsChocolateModel;
    protected $producerModel;
    protected $lotsCacauModel;
    protected $organico;
    protected $pre_processado;
    protected $chocolate_bar_pattern_grass;


    public function __construct(ChocolateBarsRepositoryInterface $chocolateModel, ProducersCacauRepositoryInterface $producerModel, LotsChocolateBarsRepositoryInterface $lotsChocolateModel, LotsCacauRepositoryInterface $lotsCacauModel)
    {
        $this->chocolateModel = $chocolateModel;
        $this->lotsChocolateModel = $lotsChocolateModel;
        $this->producerModel = $producerModel;
        $this->lotsCacauModel = $lotsCacauModel;
        $this->chocolate_bar_pattern_grass = 500;
    }


    public function barsCheck($request)
    {

        $organico = null;
        $organico = null;



        foreach ($request as $item) {

            $producer = $this->producerModel->find($item['producer_id'])->first();
            $batch = $this->lotsCacauModel->find($item['batch_id']);

            if (!$batch){
                return $this->mountReturn('Lote (' . $item['batch_id'] . ') informado não existe', '', 422);
            }

            if ($producer) {
                $producer->origen == 1 ? $this->organico = $this->organico + $item['quantity_grams'] : $this->pre_processado = $this->pre_processado + $item['quantity_grams'];
            } else {
                return $this->mountReturn('Produtor (' . $item['producer_id'] . ') informado não existe', '', 422);
            }
        }



        $sum = $this->pre_processado + $this->organico;


        if ($sum != $this->chocolate_bar_pattern_grass) {
            return $this->mountReturn('Total de gramas dos lotes (' . $sum .
                ') é diferente do valor total da barra de chocolate (' . $this->chocolate_bar_pattern_grass . ')', '', 422);
        } else {
            $result_pre_processado = ($this->pre_processado / $this->chocolate_bar_pattern_grass) * 100;
            $result_organico = ($this->organico / $this->chocolate_bar_pattern_grass) * 100;

            $result_percentage = $this->check_recipe($result_pre_processado, $result_organico);


            if ($result_percentage) {
                return $result_percentage;
            }
        }

        return '';
    }


    public function mountReturn($message, $response, $status)
    {
        return [
            'message' => $message,
            'response' => $response,
            'status' => $status
        ];
    }

    public function check_recipe($result_pre_processado, $result_organico)
    {


        if ($result_pre_processado > 10) {
            return $this->mountReturn('Quantidade de cacau pre_processado não pode ser maior do que 10% da receita', '', 422);
        }
        if ($result_organico < 90) {
            return $this->mountReturn('Quantidade de cacau orgânico não pode ser menor do que 90% da receita', '', 422);
        }
    }

    public function generate_code()
    {
        $public_identifier_code = Str::random(8);
        $chocolateModel = $this->chocolateModel->findBy('public_identifier_code', $public_identifier_code);
        if (isset($chocolateModel->id)) {
            $this->generate_code();
        }

        return $public_identifier_code;
    }


    public function store($request)
    {
        $return = $this->barsCheck($request->lots);

        if (isset($return['status'])){
            return $return;
        }
        $public_identifier_code = $this->generate_code();
        $chocolateModel = $this->chocolateModel->store([
            'public_identifier_code' => $public_identifier_code,
        ]);

        if (isset($chocolateModel->id)) {
            foreach ($request->lots as $item) {

                $quantity_percentage = ($item['quantity_grams'] / $this->chocolate_bar_pattern_grass) * 100;

                $this->lotsChocolateModel->store([
                    'quantity_grams' => $item['quantity_grams'],
                    'batch_id' => $item['batch_id'],
                    'quantity_percentage' => $quantity_percentage,
                    'id_barra' => $chocolateModel->id,
                    'producer_id' => $item['producer_id']
                ]);
            }
        } else {
            return $this->mountReturn('Aconteceu algum erro ao incluir uma nova barra', '', 500);
        }
        return $this->mountReturn('Barra cadastrada com sucesso', $chocolateModel, 200);
    }


    public function showAll()
    {
        $chocolateModel = $this->chocolateModel->all();

        $index = 0;
        foreach ($chocolateModel as $item) {
            $response['bar'] = $item;
            $response['lots'] = $this->lotsChocolateModel->findBy('id_barra', $item->id);
            $return[$index] = $response;
            $index++;
        }


        if (!isset($return)) {
            $return = null;
        }

        return $return;
    }

    public function show($id)
    {

        $chocolateModel = $this->chocolateModel->find($id);


        if(isset($chocolateModel->id)) {
            $response['bar'] = $chocolateModel->id;
            $response['lots'] = $this->lotsChocolateModel->findBy('id_barra', $chocolateModel->id);
            $return = $response;

        }
        if (!isset($return)) {
            $return = null;
        }

        return $return;
    }

    public function destroy($id)
    {
        $return = $this->chocolateModel->destroy($id);
        return $return;
    }

    public function consult_bars_public($code)
    {
        $model = new ChocolateBars;
        $chocolateModel = $this->chocolateModel->findBy('public_identifier_code',$code)->first();



        if(isset($chocolateModel->id)) {
            $lots['lots'] = $this->chocolateModel->findBarInfor($chocolateModel->id);
            return $this->mountReturn('', $lots, 200);
        }else{
            return $this->mountReturn('Barra informada não foi encontrada', $chocolateModel, 422);
        }

    }


}
