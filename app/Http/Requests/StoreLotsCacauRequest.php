<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreLotsCacauRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'description' => 'required',
            'id_producer' => 'required|integer',
            'quantity_grams' => 'required|numeric',
        ];
    }

    public function messages()
    {
        return [
            'description.required' => 'A descrição do lote é obrigatório.',
            'id_producer.required' => 'O produtor é obrigatório.',
            'quantity_grams.required' => 'A quantidade em gramas é obrigatório.',
            'quantity_grams.numeric' => 'A quantidade em gramas é obrigatório ser um numérico.',
            'id_producer.integer' => 'O produtor tem que ser um numero inteiro.',
        ];
    }

}
