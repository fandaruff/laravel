<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreChocolateBarsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

            return [
                'lots.*.producer_id' => 'required|integer',
                'lots.*.quantity_grams' => 'required|numeric',
                'lots.*.batch_id' => 'required|numeric',
            ];
        }

    public function messages()
    {
        return [
            'lots.*.quantity_grams.required' => 'È obrigatório informar a quantidade de gramas.',
            'lots.*.quantity_grams.numeric' => 'A quantidade em gramas é obrigatório ser um numérico.',
            'lots.*.producer_id.required' => 'È obrigatório informar um produtor.',
            'lots.*.producer_id.integer' => 'A quantidade em gramas é obrigatório ser um numero inteiro.',
            'lots.*.batch_id.required' => 'Obirgatório informar o lote.',
            'lots.*.batch_id.integer' => 'Lote deve ser um numero inteiro.',
        ];
    }
}
