<?php

namespace App\Http\Controllers;

use App\Models\OriginCacau;
use App\Repositories\Contracts\OriginCacauRepositoryInterface;
use Illuminate\Http\Request;

class OriginCacauController extends Controller
{
    protected $model;


    public function __construct(OriginCacauRepositoryInterface  $model)
    {
        $this->model = $model;

    }

    public function showAll()
    {
        return $this->model->all();
    }


    public function show($id)
    {
        return $this->model->find($id);
    }
}
