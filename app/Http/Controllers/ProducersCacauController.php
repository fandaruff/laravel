<?php

namespace App\Http\Controllers;

use App\Models\ProducersCacau;
use App\Repositories\Contracts\ProducersCacauRepositoryInterface;
use Illuminate\Http\Request;

class ProducersCacauController extends Controller
{
    protected $model;


    public function __construct(ProducersCacauRepositoryInterface  $model)
    {
        $this->model = $model;

    }

    public function showAll()
    {
        return $this->model->all();
    }


    public function show($id)
    {
        return $this->model->find($id);
    }

}
