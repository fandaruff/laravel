<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreLotsCacauRequest;
use App\Models\LotsCacau;
use App\Repositories\Contracts\LotsCacauRepositoryInterface;
use App\Repositories\Contracts\TesteRepositoryInterface;
use App\Service\LotsCacauService;
use Illuminate\Http\Request;

class LotsCacauController extends Controller
{

    protected $model;
    protected $lotsCacauService;

    public function __construct(LotsCacauRepositoryInterface $model, LotsCacauService $lotsCacauService)
    {
        $this->model = $model;
        $this->lotsCacauService = $lotsCacauService;
    }



    public function store(StoreLotsCacauRequest $request)
    {
        $result =  $this->lotsCacauService->lotsCheck($request);

        return response()->json(['message' => $result['message'], 'response' =>  $result['response']], $result['status']);

    }

    public function showAll()
    {
        $result = $this->model->all();
        return response()->json($result);
    }

    public function show($id)
    {
        $result = $this->model->find($id);
        return response()->json($result);
    }


    public function update(StoreLotsCacauRequest $request)
    {
        $result =  $this->lotsCacauService->lotsCheck($request);
        return response()->json(['message' => $result['message'], 'response' =>  $result['response']], $result['status']);

    }


    public function destroy($id)
    {
        $result = $this->lotsCacauService->destroyLotsCacau($id);

        return response()->json([
            'message' => $result['message'], 'response' =>  $result['response']], $result['status']);
    }

}
