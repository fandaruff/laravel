<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreChocolateBarsRequest;
use App\Models\ChocolateBars;
use App\Repositories\Contracts\ChocolateBarsRepositoryInterface;
use App\Repositories\Contracts\LotsChocolateBarsRepositoryInterface;
use App\Service\ChocolateBarsService;
use Illuminate\Http\Request;

class ChocolateBarsController extends Controller
{

    protected $model_chocolate;
    protected $model_lots;
    protected $ChocolateBarsService;

    public function __construct(ChocolateBarsRepositoryInterface $model_chocolate,LotsChocolateBarsRepositoryInterface $model_lots, ChocolateBarsService $ChocolateBarsService)
    {
        $this->model_chocolate = $model_chocolate;
        $this->model_lots = $model_lots;
        $this->ChocolateBarsService = $ChocolateBarsService;

    }


    public function store(StoreChocolateBarsRequest $request)
    {
        $result =  $this->ChocolateBarsService->store($request);
        return response()->json(['message' => $result['message'], 'response' =>  $result['response']], $result['status']);
    }



    public function showAll()
    {
        $response =  $this->ChocolateBarsService->showAll();
        return $response;
    }

    public function show($id)
    {
        $response =  $this->ChocolateBarsService->show($id);
        return $response;
    }


    public function destroy($id)
    {
        $response =  $this->ChocolateBarsService->destroy($id);
        return $response;
    }

    public function consult($code)
    {

        $response =  $this->ChocolateBarsService->consult_bars_public($code);
        return $response;
    }
}
