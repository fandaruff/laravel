<?php

use App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;




Route::group(['namespace' => Controllers::class], function(){
    Route::get('consult-bar-identifier/{code}', 'ChocolateBarsController@consult');
});


Route::group(['namespace' => Controllers::class, 'middleware' => 'client'], function(){


    // lOTS CACAU CRUD
    Route::post('lots-cacau',  'LotsCacauController@store');
    Route::put('lots-cacau',  'LotsCacauController@update');
    Route::get('lots-cacau',  'LotsCacauController@showAll');
    Route::get('lots-cacau/{id}',  'LotsCacauController@show');
    Route::delete('lots-cacau/{id}',  'LotsCacauController@destroy');

    // CHOCOLATE BARS
    Route::post('chocolate-bars',  'ChocolateBarsController@store');
    Route::get('chocolate-bars',  'ChocolateBarsController@showAll');
    Route::get('chocolate-bars/{id}',  'ChocolateBarsController@show');
    Route::delete('chocolate-bars/{id}',  'ChocolateBarsController@destroy');

    // PRODUCERS
    Route::get('producers',  'ProducersCacauController@showAll');
    Route::get('producers/{id}',  'ProducersCacauController@show');

    // PRODUCERS
    Route::get('origin',  'OriginCacauController@showAll');
    Route::get('origin/{id}',  'OriginCacauController@show');
});




